package homeworks.student;

import homeworks.student.exceptions.InvalidFirstNameException;
import homeworks.student.exceptions.InvalidLastNameException;
import homeworks.student.exceptions.InvalidMailException;
import homeworks.student.exceptions.InvalidPhoneNumberException;

import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
        Student student = new Student();

        Scanner scanner = new Scanner(System.in);
        System.out.println("Gimme first name:");
        String firstName = scanner.nextLine();
        try
        {
            student.setFirstName(firstName);
        }
        catch (InvalidFirstNameException e)
        {
            System.err.println(e.getMessage());
            System.exit(1);
        }

        System.out.println("Gimme last name:");
        String lastName = scanner.nextLine();
        try
        {
            student.setLastName(lastName);
        }
        catch (InvalidLastNameException e)
        {
            System.err.println(e.getMessage());
            System.exit(1);
        }

        System.out.println("Gimme phone number:");
        String phoneNumber = scanner.nextLine();
        try
        {
            student.setPhoneNumber(phoneNumber);
        }
        catch (InvalidPhoneNumberException e)
        {
            System.err.println(e.getMessage());
            System.exit(1);
        }

        System.out.println("Gimme mail:");
        String mail = scanner.nextLine();
        try
        {
            student.setMail(mail);
        }
        catch (InvalidMailException e)
        {
            System.err.println(e.getMessage());
            System.exit(1);
        }

        scanner.close();


        System.out.println(student);


    }
}
