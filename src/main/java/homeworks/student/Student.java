package homeworks.student;

import homeworks.student.exceptions.InvalidFirstNameException;
import homeworks.student.exceptions.InvalidLastNameException;
import homeworks.student.exceptions.InvalidMailException;
import homeworks.student.exceptions.InvalidPhoneNumberException;

public class Student {
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String mail;

    public void setFirstName(String firstName) throws InvalidFirstNameException{
            if (!firstName.matches("[A-Z][a-z]+"))
            {
                throw new InvalidFirstNameException("Invalid first name " + firstName);
            }
            else
            {
                this.firstName = firstName;
            }
    }

    public void setLastName(String lastName) throws InvalidLastNameException
    {
            if (!lastName.matches("[A-Z][a-z]+"))
            {
                throw new InvalidLastNameException("Invalid last name " + lastName);
            }
            else
            {
                this.lastName = lastName;
            }

    }

    public void setPhoneNumber(String phoneNumber) throws InvalidPhoneNumberException{
        if (!phoneNumber.matches("(\\+[0-9]{2,3} )?([0-9]{3} ?){3}"))
        {
            throw new InvalidPhoneNumberException("Invalid phone number " + phoneNumber);
        }
        else
        {
            this.phoneNumber = phoneNumber;
        }

    }

    public void setMail(String mail) throws InvalidMailException
    {

        if (!mail.matches(".+@.+"))
        {
            throw new InvalidMailException("Invalid mail " + mail);
        }
        else
        {
            this.mail = mail;
        }
    }

    @Override
    public String toString() {
        return  "Hi," + '\n' + "I am " + firstName + " " + lastName + "." + '\n' +
                "My phone number is " + phoneNumber + '\n' +
                "My mail is " + mail;
    }
    
}

