package homeworks.student.exceptions;

public class InvalidPhoneNumberException extends Exception{

    public InvalidPhoneNumberException(String message) {
        super(message);
    }
}
