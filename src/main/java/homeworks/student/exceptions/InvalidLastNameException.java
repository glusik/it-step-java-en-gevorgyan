package homeworks.student.exceptions;

public class InvalidLastNameException extends Exception {

    public InvalidLastNameException(String message) {
        super(message);
    }
}
