package homeworks.student.exceptions;

public class InvalidMailException extends Exception{

    public InvalidMailException(String message) {
        super(message);
    }

}
