package homeworks.branching;

import java.util.Scanner;

public class Task12 {
    public static void main(String[] args) {

        Scanner myObj = new Scanner(System.in);
        // Asking the user to input a six-digit number
        System.out.println("Please enter a six-digit number");
        int a = myObj.nextInt();

        // Extracting the digits of the numbers
        int sixthdigit = a % 10;
        a = a / 10;
        int fifthdigit = a % 10;
        a = a / 10;
        int fourthdigit = a % 10;
        a = a / 10;
        int thirddigit = a % 10;
        a = a / 10;
        int seconddigit = a % 10;
        a = a / 10;
        int firstdigit = a % 10;

        // Checking if the sum of first three digits equal the sum of last three digits
        if (firstdigit + seconddigit + thirddigit == fourthdigit + fifthdigit + sixthdigit)
        {
            System.out.println("Yes");
        }
        else
        {
            System.out.println("No");
        }
    }
}
