package homeworks.branching;

import java.util.Scanner;

public class Task07 {
    public static void main(String[] args) {
        Scanner myObj = new Scanner (System.in);
        //Asking the user to input a character
        System.out.println("Please enter a character");
        String input = myObj.nextLine();

        // Taking the first character of the input
        char character = input.charAt(0);

        // Setting conditions to identify the type of the character
        if (character >= '\u0041' && character <= '\u005A' || character >= '\u0061' && character <= '\u007A')
            System.out.println("Latin");
        else if (character >= '\u0410' && character <= '\u044F' || character == '\u0401' || character == '\u0451')
            System.out.println("Cyrillic");
        else if (character >= '\u0030' && character <= '\u0039')
            System.out.println("Figure");
        else
            System.out.println("Cannot define");

    }
}
