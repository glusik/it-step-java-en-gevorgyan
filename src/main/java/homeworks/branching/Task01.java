package homeworks.branching;

import java.util.Scanner;

public class Task01 {
    public static void main(String[] args) {

        Scanner myObj = new Scanner(System.in);

        System.out.println("Please enter the number of the day of the week:");

        // Asking the user to input the day number
        int dayNumber = myObj.nextInt();

        // Printing the day of the week based on the input number
        switch  (dayNumber)
        {
            case 1:
                System.out.println("Monday");
                break;
            case 2:
                System.out.println("Tuesday");
                break;
            case 3:
                System.out.println("Wednesday");
                break;
            case 4:
                System.out.println("Thursday");
                break;
            case 5:
                System.out.println("Friday");
                break;
            case 6:
                System.out.println("Saturday");
                break;
            case 7:
                System.out.println("Sunday");
                break;
            default:
                System.out.println("N/A");
        }
    }
}
