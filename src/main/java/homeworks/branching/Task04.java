package homeworks.branching;

public class Task04 {
    public static void main(String[] args) {
        // Defining the coordinates of the point
        double x = 8;
        double y = -7;


        // Identifying the quadrant where the point belongs to based on the coordinates
        if (x > 0 && y > 0)
        {
            System.out.println("The point is in the quadrant I.");
        }
        else
        {
        if (x < 0 && y < 0)
        {
            System.out.println("The point is in the quadrant II");
        }
        else
        {
        if (x < 0 && y < 0)
        {
            System.out.println("The point is in the quadrant III");
        }
        else
        {
            System.out.println("The point is in the quadrant IV");
        }
        }
        }

    }
}
