package homeworks.branching;

public class Task08 {
    public static void main(String[] args) {
        // Defining the numbers x and y
        int x = 68;
        int y = 54;

        // Checking if both x and y are even or odd
        if (x % 2 == 0 && y % 2 == 0 || x % 2 == 1 && y % 2 == 1)
            System.out.println("YES");
    }
}
