package homeworks.branching;

import java.util.Scanner;

public class Task02 {
    public static void main(String[] args) {
        Scanner myObj = new Scanner (System.in);

        // Asking the user to input the hours
        System.out.println("Please enter the number of hours from 0 - 24:");

        int hours = myObj.nextInt();

        // Printing a greeting message according the time input
        if (hours >= 0 && hours < 11)
        {
            System.out.println("Good Morning!");
        }
        else
        {
        if (hours >= 11 && hours < 19 )
        {
            System.out.println("Good Day!");
        }
        else
        {
            System.out.println("Good Evening!");
        }

        }

    }
}
