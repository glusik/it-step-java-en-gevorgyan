package homeworks.branching;

import java.util.Scanner;

public class Task06 {
    public static void main(String[] args) {

        Scanner myObj = new Scanner (System.in);
        // Asking the user to enter the year
        System.out.println("Please input the year");
        int year = myObj.nextInt();

        // Printing the number of days
        if (year % 100 == 0 && year % 400 != 0)
            System.out.println("There are 365 days in " + year);
        else if (year % 4 == 0)
            System.out.println("There are 366 days in " + year);

    }
}
