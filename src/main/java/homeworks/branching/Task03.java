package homeworks.branching;

import java.util.Scanner;

public class Task03 {
    public static void main(String[] args) {
        // Printing the list of the animals
        System.out.println("1 - dog\n2 - cat\n3 - cow\n4 - duck\n5 - bee\n6 - snake\n7 - lion\n8 - dolphin\n9 - pig\n10 - sheep");
        // Asking the user to choose an animal
        System.out.println("Please enter a number 1 - 10 to choose an animal");
        Scanner myObj = new Scanner(System.in);

        // Printing the sound the chosen animal makes
        int animal = myObj.nextInt();
        switch (animal)
        {
            case 1:
                System.out.println("Woof - woof");
                break;
            case 2:
                System.out.println("Meow - Meow");
                break;
            case 3:
                System.out.println("Moo");
                break;
            case 4:
                System.out.println("Quack - Quack");
                break;
            case 5:
                System.out.println("Buzz");
                break;
            case 6:
                System.out.println("Hiss");
                break;
            case 7:
                System.out.println("Roar");
                break;
            case 8:
                System.out.println("Click");
                break;
            case 9:
                System.out.println("Oink");
                break;
            case 10:
                System.out.println("Mee");
                break;
            default:
                System.out.println("Animal not found");


        }
    }
}
