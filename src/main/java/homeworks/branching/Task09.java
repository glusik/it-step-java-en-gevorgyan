package homeworks.branching;

public class Task09 {
    public static void main(String[] args) {
        // Defining the coordinates of the beginning point
        int xb = 12;
        int yb = 2;

        // Defining the coordinates of the ending point

        int xe = 23;
        int ye = 9;

        // Comparing the coordinates and printing about how the segment moves
        if (ye > yb && xb != xe)
            System.out.println("The segment is ascend");
        else if (ye < yb && xb != xe)
            System.out.println("The segment is descend");
        else if (xb == xe)
            System.out.println("The segment is upright");
        else if (yb == ye)
            System.out.println("The segment is smooth");
    }
}
