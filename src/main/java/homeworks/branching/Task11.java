package homeworks.branching;

import java.util.Scanner;

public class Task11 {
    public static void main(String[] args) {
        Scanner myObj = new Scanner (System.in);

        //Asking the user to input the coefficients of the equation
        System.out.println("Please enter the value of a");
        double a = myObj.nextDouble();
        System.out.println("Please enter the value of b");
        double  b = myObj.nextDouble();
        System.out.println("Please enter the value of c");
        double c = myObj.nextDouble();

        // Writing the formulas of the dicriminant and the roots
        double D = b*b - 4*a*c;
        double x1 = (-b + Math.sqrt(D))/(2*a);
        double x2 = (-b - Math.sqrt(D))/(2*a);

        // Printing the roots of the equations, if any
        if (a != 0 && D > 0)
        {
        System.out.println("There are two roots for this equation:\n" +
                    "x1 = " + x1 + '\n' + "x2 = " + x2);
        }
        else if (a != 0 && D == 0)
        {
            System.out.println("The root of the equation is " + x1);
        }
        else if (a != 0 && D < 0)
        {
            System.out.println("The equation has no roots.");
        }
        else
        {
            double x = -c/b;
            System.out.println("The root of the equation is " + x);
        }



    }
}
