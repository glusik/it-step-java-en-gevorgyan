package homeworks.branching;

import java.util.Scanner;

public class Task05 {
    public static void main(String[] args) {

        // Asking the user to input the day, month and year of birth
        Scanner myObj = new Scanner (System.in);
        System.out.println("Please input the day of your birth");
        int day = myObj.nextInt();
        System.out.println("Please input the month of your birth");
        int month = myObj.nextInt();
        System.out.println("Please input the year of your birth");
        int year = myObj.nextInt();

        // Setting the conditions to identify the zodiac sign
        if (day >= 21 && day <=31 && month == 3 || day >= 1 && day <= 19 && month == 4)
            System.out.println("Zodiac Sign: Aries");
        else if (day >= 20 && day <=30 && month == 4 || day >= 1 && day <= 20 && month == 5)
                    System.out.println("Zodiac Sign: Taurus");
        else if (day >= 21 && day <=31 && month == 5 || day >= 1 && day <= 20 && month == 6)
                    System.out.println("Zodiac Sign: Gemini");
        else if (day >= 21 && day <=30 && month == 6 || day >= 1 && day <= 22 && month == 7)
                    System.out.println("Zodiac sign: Cancer");
        else if (day >= 23 && day <=31 && month == 7 || day >= 1 && day <= 22 && month == 8)
                    System.out.println("Zodiac sign: Leo");
        else if (day >= 23 && day <=31 && month == 8 || day >= 1 && day <= 22 && month == 9)
                    System.out.println("Zodiac sign: Virgo");
        else if (day >= 23 && day <=30 && month == 9 || day >= 1 && day <= 22 && month == 10)
                    System.out.println("Zodiac sign: Libra");
        else if (day >= 23 && day <=31 && month == 10 || day >= 1 && day <= 21 && month == 11)
                    System.out.println("Zodiac sign: Scorpio");
        else if (day >= 22 && day <=30 && month == 11 || day >= 1 && day <= 21 && month == 12)
                    System.out.println("Zodiac sign: Sagittarius");
        else if (day >= 22 && day <=31 && month == 12 || day >= 1 && day <= 19 && month == 1)
                    System.out.println("Zodiac sign: Capricorn");
        else if (day >= 20 && day <=31 && month == 1 || day >= 1 && day <= 18 && month == 2)
                    System.out.println("Zodiac sign: Aquarius");
        else if (day >= 19 && day <=29 && month == 2 || day >= 1 && day <= 20 && month == 3)
                    System.out.println("Zodiac sign: Pisces");

        // Setting the conditions to identify the year according to the Chinese calendar
        switch (year % 12)
        {
            case 0:
                System.out.println("Year: Monkey");
                break;
            case 1:
                System.out.println("Year: Rooster");
                break;
            case 2:
                System.out.println("Year: Dog");
                break;
            case 3:
                System.out.println("Year: Pig");
                break;
            case 4:
                System.out.println("Year: Rat");
                break;
            case 5:
                System.out.println("Year: Ox");
                break;
            case 6:
                System.out.println("Year: Tiger");
                break;
            case 7:
                System.out.println("Year: Rabbit");
                break;
            case 8:
                System.out.println("Year: Dragon");
                break;
            case 9:
                System.out.println("Year: Snake");
                break;
            case 10:
                System.out.println("Year: Horse");
                break;
            case 11:
                System.out.println("Year: Goat");
                break;

        }

    }
}
