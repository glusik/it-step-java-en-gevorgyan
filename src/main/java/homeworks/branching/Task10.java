package homeworks.branching;

public class Task10 {
    public static void main(String[] args) {
        // Defining the number of a specific apartment
        int N = 13;

        // Defining the amount of apartments on each floor
        int M = 3;

        // Printing the location of the entrance
        switch (N % M)
        {
            case 0:
                System.out.println("The entrance is the last one");
                break;
            case 1:
                System.out.println("The entrance is the first one from the left");
                break;
            case 2:
                System.out.println("The entrance is the second one from the left");
                break;
            case 3:
                System.out.println("The entrance is the third one from the left");
                break;
        }

    }
}
