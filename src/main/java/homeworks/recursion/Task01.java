package homeworks.recursion;

import java.util.Scanner;

public class Task01 {
    public static void main(String[] args) {
        // getting the value of A
        Scanner myObj1 = new Scanner(System.in);
        System.out.println("Please enter the value of integer A");
        int integerA = myObj1.nextInt();

        // getting the value of B
        Scanner myObj2 = new Scanner(System.in);
        System.out.println("Please enter the value of integer B");
        int integerB = myObj2.nextInt();


        numbersWithRecursion(integerA,integerB);
    }

    public static void numbersWithRecursion(int a, int b) {

        if (a < b)
        {
            System.out.print(a + " ");
            numbersWithRecursion(a+1,b);
        }
        else if (a > b)
        {
            System.out.print(a + " ");
            numbersWithRecursion(a-1,b);
        }
        else
            System.out.println(b);
       }

}
