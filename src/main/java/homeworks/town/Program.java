package homeworks.town;

import homeworks.town.buildings.*;

public class Program {
    public static void main(String[] args) {
        Citizen citizen1 = new Citizen();
        Citizen citizen2 = new Citizen();
        Citizen citizen3 = new Citizen();
        Citizen citizen4 = new Citizen();
        Citizen citizen5 = new Citizen();
        Citizen citizen6 = new Citizen();
        Citizen citizen7 = new Citizen();
        Citizen citizen8 = new Citizen();
        Citizen citizen9 = new Citizen();
        Citizen citizen10 = new Citizen();

        Citizen[] family1 = new Citizen[3];
        Citizen[] family2 = new Citizen[3];
        Citizen[] family3 = new Citizen[2];
        Citizen[] family4 = new Citizen[2];

        House house1 = new House(100,family1);
        House house2 = new House(100,family2);
        House house3 = new House(100,family3);
        House house4 = new House(100,family4);

        School school1 = new School(100);
        School school2 = new School(120);

        Shop shop1 = new Shop(50);
        Shop shop2 = new Shop(70);
        Shop shop3 = new Shop(80);
        Shop shop4 = new Shop(80);

        Library library = new Library(80);

        Cinema cinema1 = new Cinema(120);
        Cinema cinema2 = new Cinema(90);

        Street street1 = new Street("first",6);
        Street street2 = new Street("second",6);
        Street street3 = new Street("third",6);
        Street street4 = new Street("fourth",6);

        street1.placeBuilding(1,house2);
        street1.placeBuilding(3,cinema1);
        street1.placeBuilding(5,shop1);

        street2.placeBuilding(0,house3);
        street2.placeBuilding(1,house1);
        street2.placeBuilding(4,school2);

        street3.placeBuilding(2,library);
        street3.placeBuilding(3,school1);
        street3.placeBuilding(4,cinema2);

        street4.placeBuilding(0,house4);
        street4.placeBuilding(1,shop3);
        street4.placeBuilding(4,shop2);
        street4.placeBuilding(5,shop4);

        Street[] streets = {street1,street2,street3,street4};

        Town town = new Town("Brno", streets);

        town.printFullInfo();















    }
}
