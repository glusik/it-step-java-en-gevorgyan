package homeworks.town;

import homeworks.town.buildings.Building;
import homeworks.town.buildings.House;

public class Street {
    private String name;
    private int maximalNumberOfBuildings;
    private Building[] buildings;

    public Street(String name, int maximalNumberOfBuildings) {
        this.name = name;
        this.maximalNumberOfBuildings = maximalNumberOfBuildings;
        this.buildings = new Building[maximalNumberOfBuildings];
    }

    public int getBuildCount(){
        int buildCount = 0;
        for (int index = 0; index < buildings.length;index++){
            if (buildings[index] == null) continue;
            buildCount = buildCount + 1;

        }
        return buildCount;
    }

    public int getEmptyPlacesCount(){
       int emptyPlacesCount = 0;
       for (int index = 0; index < buildings.length;index++){
           if (buildings[index] != null) {continue;}
           emptyPlacesCount = emptyPlacesCount + 1;
       }
       return emptyPlacesCount;
    }

    public int getNumberOfCitizen(){
        int noOfCitizens = 0;
        for (int index = 0; index < buildings.length; index++)
        {
            if (buildings[index] instanceof House) {
                House house = (House) buildings[index];
                noOfCitizens = noOfCitizens + house.family.length;
            }
        }
        return noOfCitizens;
    }

    public int  getBuildUpArea(){
        int buildUpArea = 0;
        for (int index = 0; index < buildings.length;index++){
            if (buildings[index] == null) continue;
            buildUpArea = buildUpArea + buildings[index].getBuildUpArea();

        }
        return buildUpArea;
    }


    public void placeBuilding(int numberOfBuilding, Building building){

        if (buildings[numberOfBuilding] == null){
            buildings[numberOfBuilding] = building;
        }
        else {
            System.err.println("The place is not available");
        }
        }

    public void printInfo(){
        System.out.println("Street " + name + '\n' + ">>>>>>>>>>>>>>>>>>>>");
        System.out.println("Build up area " + getBuildUpArea());
        System.out.println("Number of buildings " + getBuildCount());
        System.out.println("Number of empty places " + getEmptyPlacesCount());
        System.out.println("Number of citizens " + getNumberOfCitizen());
        System.out.println();
    }

    public void printMarkMap(){
        for (int index = 0; index <this.buildings.length; index++){
            if (this.buildings[index] == null) {
                System.out.print("[] ");
            }
            else {
                System.out.print("[" + this.buildings[index].getMarker() + "] ");
            }
        }
    }

    }

