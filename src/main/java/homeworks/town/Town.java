package homeworks.town;

import homeworks.town.buildings.Building;

public class Town {
    private String name;
    private Street[] streets;

    public Town(String name, Street[] streets) {
        this.name = name;
        this.streets = streets;
    }

    private double getNumberOfCitizen(){
        double noOfCitizens = 0;
        for (int index = 0; index < streets.length;index++){
            noOfCitizens = noOfCitizens + streets[index].getNumberOfCitizen();
        }
        return noOfCitizens;
    }

    private double getEmptySpacesCount(){
        double emptySpaces = 0;
        for (int index = 0; index < streets.length;index++){
            emptySpaces = emptySpaces + streets[index].getEmptyPlacesCount();
        }
        return emptySpaces;
    }

    private double getBuildingCount(){
        double buildingCount = 0;
        for (int index = 0; index < streets.length;index++){
            buildingCount = buildingCount + streets[index].getBuildCount();
        }
        return buildingCount;
    }

    private  double getBuildUpArea(){
        double buildUpArea = 0;
        for (int index = 0; index < streets.length;index++){
            buildUpArea = buildUpArea + streets[index].getBuildUpArea();
        }
        return buildUpArea;
    }

    public void printMarkMap(){
        for (int index = 0; index < streets.length;index++){
            streets[index].printMarkMap();
            System.out.println();
            System.out.println();
        }

    }

    public void printInfo(){
        System.out.println("Town " + name);
        System.out.println("Build up area " + getBuildUpArea());
        System.out.println("Number of buildings " + getBuildingCount());
        System.out.println("Number of empty places " + getEmptySpacesCount());
        System.out.println("Number of citizens " + getNumberOfCitizen());
        System.out.println();
    }

    public void printFullInfo(){
        printInfo();

        for (int index = 0;index < streets.length;index++){
            streets[index].printInfo();
        }
        printMarkMap();
    }
}
