package homeworks.town.buildings;

public class Shop extends Building {
    public Shop(int buildUpArea) {
        super(buildUpArea);
    }

    @Override
    public String getMarker() {
        return "$";
    }
}
