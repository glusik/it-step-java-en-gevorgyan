package homeworks.town.buildings;

public class Library extends Building{
    public Library(int buildUpArea) {
        super(buildUpArea);
    }

    @Override
    public String getMarker() {
        return "L";
    }
}
