package homeworks.town.buildings;

public class Cinema extends Building {

    public Cinema(int buildUpArea) {
        super(buildUpArea);
    }

    @Override
    public String getMarker() {
        return "C";
    }
}