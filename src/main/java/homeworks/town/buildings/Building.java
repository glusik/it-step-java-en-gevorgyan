package homeworks.town.buildings;

abstract public class Building {
    private int buildUpArea;

    public Building(int buildUpArea) {
        this.buildUpArea = buildUpArea;
    }

    public int getBuildUpArea() {
        return buildUpArea;
    }

    public abstract String getMarker();
}
