package homeworks.town.buildings;

public class School extends Building {
    public School(int buildUpArea) {
        super(buildUpArea);
    }

    @Override
    public String getMarker() {
        return "S";
    }
}
