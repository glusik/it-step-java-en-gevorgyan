package homeworks.town.buildings;

import homeworks.town.Citizen;

public class House extends Building {
    public House(int buildUpArea) {
        super(buildUpArea);
    }
    public Citizen[] family;

    public House(int buildUpArea, Citizen[] family) {
        super(buildUpArea);
        this.family = family;
    }

    @Override
    public String getMarker() {
        return "H";
    }
}
