package homeworks.card;

import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
        Card card = new Card();
        Scanner scanner = new Scanner(System.in);

        while (true)
        {
            System.out.println("Choose the command: " + '\n' + "A\tAdd item to the card" + '\n' + "P\tPrint the card" + '\n' + "E\tExit the program");
            String command = scanner.nextLine();

            switch (command)
            {
                case "E":
                    break;
                case "P":
                    System.out.println(card);
                    break;
                case "A":
                    help(scanner,card);
                    break;
                default:
                    System.out.println("Invalid command");
            }

            if (command.equals("E"))
            {
                System.out.println("Bye Bye");
                return;
            }

        }

    }

    public static void help(Scanner scanner, Card card){
        System.out.println("Gimme item name:");
        String name = scanner.nextLine();

        System.out.println("Gimme item price:");
        String priceStr = scanner.nextLine();

        try
        {
            Item item = new Item(name,Double.parseDouble(priceStr));
            card.addItem(item);
            System.out.println("Item " + item + " was added into the card.");

        }
        catch (NumberFormatException e)
        {
            System.out.println("Invalid price");
        }

        catch (ItemDuplicateException e)
        {
            System.err.println(e.getMessage());
        }
    }
}
