package homeworks.card;

public class ItemDuplicateException extends Exception {
    public ItemDuplicateException(String message) {
        super(message);
    }
}
