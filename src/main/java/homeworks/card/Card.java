package homeworks.card;
import java.util.HashSet;

public class Card {
    private HashSet<Item> items;

    public Card() {
        this.items = new HashSet();
    }

    public void addItem(Item item) throws ItemDuplicateException
    {
        if (items.contains(item))
        {
            throw new ItemDuplicateException("There is already " + item + "item in the card");
        }
        else
        {
            items.add(item);
        }
    }

    @Override
    public String toString() {

        if (items.size() == 0)
        {
            return "Empty card";
        }
        else
        {
            double itemPriceSum = 0;
            StringBuilder string = new StringBuilder();
            for (Item item : items)
            {
                itemPriceSum += item.getPrice();
                string.append(item);
                string.append('\n');

            }

            return "Card with " + items.size() + " items" + '\n' + "----------------" +'\n' +
                    string + '\n' + "----------------" + '\n' + itemPriceSum + "$";
        }

    }
}


