package homeworks.homework11.task02;

import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Map;
import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Map<String, String> userMap = new HashMap<>();
        String commands = "Please choose a command by entering a corresponding number:" + '\n' + "1. Add a new user" + '\n' +
                "2. Delete a user" + '\n' + "3. Check if the user exists" + '\n' + "4. Change the username of the user" + '\n' +
                "5. Change the password of a user";

        while (true) {
            try {
                    System.out.println(commands);

                    int command = Integer.parseInt(scanner.nextLine());

                    switch (command) {
                        case 1:
                            System.out.println("Please enter your username");
                            String username = scanner.nextLine();
                            if (username.isEmpty())
                            {
                                System.out.println("No username was entered.");
                            }
                            else if (userMap.containsKey(username))
                            {
                                System.out.println("The username already exists. Please try again");
                            }
                            else
                            {
                                System.out.println("Please enter your password");
                                String password = scanner.nextLine();
                                if (password.isEmpty())
                                {
                                    System.out.println("No password was entered. Please try again");
                                }
                                else
                                {
                                    userMap.put(username, password);
                                }
                            }
                            break;
                        case 2:
                            System.out.println("Please enter the username you want to remove");
                            String delUser = scanner.nextLine();
                            if (delUser.isEmpty()) {
                                System.out.println("No username was entered");
                            } else if (userMap.containsKey(delUser)) {
                                userMap.remove(delUser);
                                System.out.println("The username was deleted");
                            } else {
                                System.out.println("The username does not exist");
                            }
                            break;
                        case 3:
                            System.out.println("Please enter the username you want to check");
                            String checkUsername = scanner.nextLine();
                            if (checkUsername.isEmpty()) {
                                System.out.println("No username was entered");
                            } else if (userMap.containsKey(checkUsername)) {
                                System.out.println("The user with this username already exists");
                            } else {
                                System.out.println("There is no user with such username");
                            }
                            break;
                        case 4:
                            System.out.println("Please enter the current username");
                            String currentUsername = scanner.nextLine();
                            if (currentUsername.isEmpty()) {
                                System.out.println("No username was entered. Please try again");
                            } else {
                                System.out.println("Please enter a new username");
                                String newUsername = scanner.nextLine();
                                if (newUsername.isEmpty()) {
                                    System.out.println("No username was entered.Please try again");
                                } else if (userMap.containsKey(newUsername)) {
                                    System.out.println("The username already exists. Please try again.");
                                } else {
                                    String userPassword = userMap.get(currentUsername);
                                    userMap.remove(currentUsername);
                                    userMap.put(newUsername, userPassword);
                                    System.out.println("The username has been changed.");
                                }
                            }
                            break;
                        case 5:
                            System.out.println("Please enter your current password");
                            String currentPass = scanner.nextLine();
                            if (currentPass.isEmpty())
                            {
                                System.out.println("No password was entered. Please try again");
                            }
                            else
                                {
                                System.out.println("Please enter a new password");
                                String newPass = scanner.nextLine();
                                if (newPass.isEmpty()) {
                                    System.out.println("No password was entered. Please try again");
                                } else {
                                    String userName = "";
                                    for (Map.Entry<String, String> entry : userMap.entrySet()) {
                                        if (entry.getValue().equals(currentPass)) {
                                            userName = entry.getKey();
                                        }
                                    }
                                    userMap.replace(userName, currentPass, newPass);
                                    System.out.println("The password has been changed.");
                                    }
                                }
                            break;
                        default:
                            System.out.println("Invalid command");
                    }
            }
                catch (NumberFormatException e)
                {
                    System.out.println("Wrong format. Please enter a number");
                }
        }
    }
}
