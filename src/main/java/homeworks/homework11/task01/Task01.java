package homeworks.homework11.task01;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Task01 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter numbers followed by space");
        ArrayList<Integer> list = new ArrayList<>();

        while (true)
        {
            String string  = scanner.nextLine();
            if (!string.matches("([0-9] )+"))
            {
                System.out.println("Wrong format. Please enter a number");
                continue;
            }
            String[] numbers = string.split(" "); // creating an array with integers from input
            for (int i = 0; i < numbers.length; i++)
            {
                list.add(Integer.parseInt(numbers[i])); // converting the array to ArrayList
            }
        break;
        }

        String commands = "1. Add an item to the list" + '\n' + "2. Delete an item from the list" +'\n' +
                "3. Display the list content" + '\n' + "4. Check if a value is in the list" + '\n' + "5. Replace the value in the list";

       while (true)
       {
           try
           {
               System.out.println("Please choose a command by entering a corresponding number:" + '\n' + commands);
               int userCommand = Integer.parseInt(scanner.nextLine());

              // defining the functions of commands
              switch (userCommand) {
                  case 1:
                      System.out.println("Please enter a new number to add");
                      int newNum = scanner.nextInt();
                      list.add(newNum);
                      break;
                  case 2:
                      System.out.println("Please enter the number you want to delete from the list");
                      int delNum = scanner.nextInt();
                      list.remove(delNum);
                      break;
                  case 3:
                      System.out.println(Arrays.toString(list.toArray()));
                      break;
                  case 4:
                      System.out.println("Please enter the number you want to search in the list");
                      int findNum = scanner.nextInt();
                      if (list.contains(findNum)) {
                          System.out.println("The number is in the list");
                      } else {
                          System.out.println("The number is not in the list");
                      }
                      break;
                  case 5:
                      System.out.println("Please enter the number you want to take away from the list");
                      int numToRemove = scanner.nextInt();
                      System.out.println("Please enter the number you want to replace it with");
                      int numToAdd = scanner.nextInt();
                      list.set(list.indexOf(numToRemove), numToAdd);
                      break;
                  default:
                      System.out.println("Invalid command");

              }
              break;
          }
          catch (NumberFormatException e)
          {
              System.out.println("Wrong format. Please enter a number");
          }
       }
    }
}
