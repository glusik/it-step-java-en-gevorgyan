package homeworks.strings;

public class Task01 {
    public static void main(String[] args) {
        String s = new String("Life is beautiful");
        char character = 'L';
        boolean occurrence = s.contains(String.valueOf(character));
        int index = s.indexOf(character);

        if (occurrence)
        {
            System.out.print("The indexes of the character " + character + " in the string are: ");
            while (index >= 0 && index <= s.length())
            {
                System.out.print(index + " ");
                index = s.indexOf(character,index + 1);
            }
        }
        else
           {
                System.out.println("There are no matches of the character " + character + " the string.");
            }
    }
}
