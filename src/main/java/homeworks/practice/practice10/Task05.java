package homeworks.practice.practice10;

import java.util.Scanner;

public class Task05 {
    public static void main(String[] args) {
        int[][] array = new int[3][3];

        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter the first element of the first nested array");
        array[0][0] = scanner.nextInt();

        System.out.println("Please enter the second element of the first nested array");
        array[0][1] = scanner.nextInt();

        System.out.println("Please enter the third element of the first nested array");
        array[0][2] = scanner.nextInt();


        System.out.println("Please enter the first element of the second nested array");
        array[1][0] = scanner.nextInt();

        System.out.println("Please enter the second element of the second nested array");
        array[1][1] = scanner.nextInt();

        System.out.println("Please enter the third element of the second nested array");
        array[1][2] = scanner.nextInt();


        System.out.println("Please enter the first element of the third nested array");
        array[2][0] = scanner.nextInt();

        System.out.println("Please enter the second element of the third nested array");
        array[2][1] = scanner.nextInt();

        System.out.println("Please enter the third element of the third nested array");
        array[2][2] = scanner.nextInt();

        scanner.close();

        // calculating sum of the elements
        int sum = 0;
        for (int[] nestedArray : array)
        {
            for (int element : nestedArray)
            {
                sum = sum +element;
            }
        }

        //calculating the count of the elements
        int countOfElements = 0;
        for (int[] nestedArray : array )
        {
            for (int element : nestedArray )
            {
                countOfElements++;
            }
        }

        // calculating the arithmetic mean of the elements
        int arithmeticMean = sum/countOfElements;

        //printing the array
        for (int[] nestedArray : array )
        {
            for (int element : nestedArray )
            {
                System.out.print(element + " ");
            }
            System.out.println();
        }

        System.out.println("The sum of the array elements is " + sum);
        System.out.println("The arithmetic mean of the array elements is " + arithmeticMean);
    }
}
