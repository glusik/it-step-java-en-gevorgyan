package homeworks.practice.practice12;

import java.util.Scanner;

public class Task02 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter the length of the square side");
        int side = scanner.nextInt();
        scanner.close();

        for (int a = 1; a <= side; a++) {
            for (int b = 1; b <= side; b++) {
                if (a == 1 || a == side) {
                    System.out.print("*");
                } else {
                    if (b != 1 && b != side) {
                        System.out.print(" ");
                    } else {
                        System.out.print("*");
                    }

                }
            }

            System.out.println();
        }

        }
    }
