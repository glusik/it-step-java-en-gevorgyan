package homeworks.practice.practice12;

import java.util.Scanner;

public class Task01 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter the length of the square side");
        int side = scanner.nextInt();
        scanner.close();

        for (int a = 1; a <= side; a++)
        {
           for (int b = 1; b <= side;b++)
           {
               System.out.print("*");
           }
            System.out.println();
        }
    }
}
