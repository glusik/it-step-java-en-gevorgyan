package homeworks.practice.practice8;

import java.util.Scanner;

public class Task01 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter the first number");
        int num1 = scanner.nextInt();

        System.out.println("Please enter the second number");
        int num2 = scanner.nextInt();

        System.out.println("Please enter the third number");
        int num3 = scanner.nextInt();
        scanner.close();

        int newNum = num1*100 + num2*10 + num3;
        System.out.println(newNum);
    }
}
