package homeworks.practice.practice8;

import java.util.Scanner;

public class Task03 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter the number of meters");
        int number = scanner.nextInt();

        System.out.println("Please enter which unit do you want to convert the meters to: centimeters, decimeters, millimeters or miles");
        String unitOfMeasure = scanner.next();
        scanner.close();

        double result;

        if (unitOfMeasure.equals("centimeters"))
        {
            result = number * 100;
            System.out.println(result);
        }
        else if (unitOfMeasure.equals("decimeters"))
        {
            result = number * 10;
            System.out.println(result);
        }
        else if (unitOfMeasure.equals("millimeters"))
        {
            result = number * 1000;
            System.out.println(result);
        }
        else if (unitOfMeasure.equals("miles"))
        {
            result = number * 0.000621371;
            System.out.println(result);
        }
        else
        {
            System.err.println("Invalid unit of measure");
        }

    }
}
