package homeworks.practice.practice8;

import java.util.Scanner;

public class Task04 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter the base length of the triangle");
        int base = scanner.nextInt();

        System.out.println("Please enter the altitude of the triangle");
        int altitude = scanner.nextInt();
        scanner.close();

        double area = base * altitude /2;
        System.out.println(area);
    }
}
