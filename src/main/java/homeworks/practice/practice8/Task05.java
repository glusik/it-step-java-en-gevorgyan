package homeworks.practice.practice8;

import java.util.Scanner;

public class Task05 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter a four-digit number");
        int number = scanner.nextInt();
        scanner.close();

        if (number <= 999)
        {
            System.out.println("The number has less than four digits");
        }
        else if (number >= 10000)
        {
            System.out.println("The number has more than four digits");
        }

        int fourthDigit = number % 10;
        int threeDigitNumber = number / 10;

        int thirdDigit = threeDigitNumber % 10;
        int twoDigitNumber = threeDigitNumber / 10;

        int secondDigit = twoDigitNumber % 10;
        int firstDigit = twoDigitNumber / 10;

        int reversedNumber = fourthDigit * 1000 + thirdDigit * 100 + secondDigit * 10 + firstDigit;
        System.out.println(reversedNumber);
    }
}
