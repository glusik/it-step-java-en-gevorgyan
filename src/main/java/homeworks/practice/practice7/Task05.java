package homeworks.practice.practice7;

import java.util.Scanner;

public class Task05 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter the length of the square side");
        int length = scanner.nextInt();
        scanner.close();


        int area = length * length;
        System.out.println("The are of the square is " + area);
    }
}
