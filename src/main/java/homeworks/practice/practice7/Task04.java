package homeworks.practice.practice7;

import java.util.Scanner;

public class Task04 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter a number");
        int number = scanner.nextInt();

        System.out.println("Please enter the percentage you want to calculate");
        int percentage = scanner.nextInt();

        scanner.close();

        double result = number * percentage / 100;
        System.out.println("The " + percentage + "% of " + number + " is " + result);
    }
}
