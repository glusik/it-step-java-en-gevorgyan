package homeworks.practice.practice7;

import java.util.Scanner;

public class Task03 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter the first number");
        int num1 = scanner.nextInt();

        System.out.println("Please enter the second number");
        int num2 = scanner.nextInt();
        scanner.close();

        int sum = num1 + num2;
        int difference = Math.abs(num1 - num2);
        int product = num1 * num2;

        System.out.println("The sum of the numbers is " + sum);
        System.out.println("The difference of the numbers is " + difference);
        System.out.println("The product of the numbers is " + product);
    }
}
