package homeworks.practice.practice9;

import java.util.Scanner;

public class Task06 {
    public static void main(String[] args) {

        int[] array = new int[5];

        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter the first element of the array");
        array[0] = scanner.nextInt();

        System.out.println("Please enter the second element of the array");
        array[1] = scanner.nextInt();

        System.out.println("Please enter the third element of the array");
        array[2] = scanner.nextInt();

        System.out.println("Please enter the fourth element of the array");
        array[3] = scanner.nextInt();

        System.out.println("Please enter the fifth element of the array");
        array[4] = scanner.nextInt();
        scanner.close();

        //calculating the sum of the array elements
        int sum = 0;
        for (int element : array)
        {
            sum = sum + element;
        }

        //calculating the arithmetic mean of the array elements
        int arithmeticMean = sum/array.length;

        //printing the array
        for (int index = 0; index < array.length;index++)
        {
            System.out.print(array[index]+" ");
        }
        System.out.println();
        System.out.println("The sum of the array elements is " + sum);
        System.out.println("The arithmetic mean of the array elements is " + arithmeticMean);
    }
}
