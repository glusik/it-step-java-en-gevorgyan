package homeworks.practice.practice9;

import java.util.Scanner;

public class Task03 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter a sentence");
        String sentence = scanner.nextLine();

        System.out.println("Please enter a search word");
        String searchWord = scanner.nextLine();

        System.out.println("Please enter a replacement word");
        String replaceWord = scanner.nextLine();
        scanner.close();

        System.out.println(sentence.replace(searchWord,replaceWord));
    }
}
