package homeworks.practice.practice4;

import java.util.Scanner;

public class Task05 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter a number from range 1 to 100");

        int number = scanner.nextInt();
        scanner.close();

        if (number < 1 || number > 100)
        {
            System.err.println("The number is out of the specified range");
        }
        else if (number % 3 == 0 && number % 5 != 0)
        {
            System.out.println("Fizz");
        }
        else if (number % 5 == 0 && number % 3 != 0)
        {
            System.out.println("Buzz");
        }
        else if (number % 3 == 0 && number % 5 == 0)
        {
            System.out.println("Fizz Buzz");
        }
        else if (number % 3 != 0 && number % 5 != 0)
        {
            System.out.println(number);
        }

    }
}
