package homeworks.practice.practice4;

import java.util.Scanner;

public class Task02 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter a six-digit number");
        int number = scanner.nextInt();
        scanner.close();

        int sixthDigit = number % 10;
        int fiveDigitNumber = number / 10;

        int fifthDigit = fiveDigitNumber % 10;
        int fourDigitNumber = fiveDigitNumber / 10;

        int fourthDigit = fourDigitNumber % 10;
        int threeDigitNumber = fourDigitNumber / 10;

        int thirdDigit = threeDigitNumber % 10;
        int twoDigitNumber = threeDigitNumber / 10;

        int secondDigit = twoDigitNumber % 10;
        int firstDigit = twoDigitNumber / 10;

        int reversedNumber = sixthDigit*100000 + fifthDigit*10000 + thirdDigit*1000 + fourthDigit*100 + secondDigit*10 + firstDigit;
        System.out.println(reversedNumber);
    }
}
