package homeworks.practice.practice6;

public class Task02 {
    public static void main(String[] args) {
        int[] array = {67,3,24,64,86};
        System.out.println(maximumNumber(array));

    }

    public static int maximumNumber(int[] array)
    {
        int max = array[0];
        for (int index = 0; index < array.length; index++)
        {
            if (array[index] > max)
            {
                max = array[index];
            }
        }
        return max;
    }
}
