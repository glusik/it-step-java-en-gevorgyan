package homeworks.practice.practice6;

public class Task01 {
    public static void main(String[] args) {
        int[] array = {5,8,43,51,94};
        System.out.println(sumCalculation(array));
    }

    public static int sumCalculation(int[] array)
    {
        int sum = 0;
        for (int element : array)
        {
            sum = sum + element;
        }
        return sum;
    }
}
