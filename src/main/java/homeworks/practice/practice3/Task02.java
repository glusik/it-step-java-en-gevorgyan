package homeworks.practice.practice3;

import java.util.Scanner;

public class Task02 {
    public static void main(String[] args) {
        System.out.println("Please enter the first number");
        Scanner scanner = new Scanner(System.in);
        if (!scanner.hasNextDouble())
        {
            System.out.println("Invalid number");
        }
        double number1 = scanner.nextDouble();
        System.out.println("Please enter the second number");
        if (!scanner.hasNextDouble())
        {
            System.out.println("Invalid number");
        }
        double number2 = scanner.nextDouble();
        System.out.println("Please enter the third number");
        if (!scanner.hasNextDouble())
        {
            System.out.println("Invalid number");
        }
        double number3 = scanner.nextDouble();


        System.out.println("Please press * if you want to multiply the numbers or + if you want to sum them");
        String operation = scanner.next();
        scanner.close();
        char operationChar = operation.charAt(0);


        double result;

        if (operationChar == '*')
        {
            result = number1 * number2 * number3;
            System.out.println(result);
        }
        else if (operationChar == '+')
        {
            result = number1 + number2 + number3;
            System.out.println(result);
        }
        else
        {
            System.out.println("Invalid operation");
        }

    }
}
