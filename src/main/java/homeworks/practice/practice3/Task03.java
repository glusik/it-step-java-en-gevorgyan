package homeworks.practice.practice3;

import java.util.Scanner;

public class Task03 {
    public static void main(String[] args) {
        System.out.println("Please enter the first number");
        Scanner scanner = new Scanner(System.in);
        if (!scanner.hasNextDouble())
        {
            System.out.println("Invalid number");
        }
        double number1 = scanner.nextDouble();
        System.out.println("Please enter the second number");
        if (!scanner.hasNextDouble())
        {
            System.out.println("Invalid number");
        }
        double number2 = scanner.nextDouble();
        System.out.println("Please enter the third number");
        if (!scanner.hasNextDouble())
        {
            System.out.println("Invalid number");
        }
        double number3 = scanner.nextDouble();
        scanner.close();

        double min = number1;
        double max = number1;
        double avg = (number1 + number2 + number3)/3;

        //finding the lowest number
        if (number2 <= min && number2 <= number3)
        {
            min = number2;
        }
        else if (number3 <= min && number3 <= number2)
        {
            min = number3;
        }

        //finding the largest number
        if (number2 >= max && number2 >= number3)
        {
            max = number2;
        }
        else if (number3 >= max && number3 >= number2 )
        {
            max = number3;
        }

        System.out.println("The lowest number is " + min);
        System.out.println("The largest number is " + max);
        System.out.println("The arithmetic mean of the numbers is " + avg);
    }




}
