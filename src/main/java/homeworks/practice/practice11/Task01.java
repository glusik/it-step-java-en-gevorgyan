package homeworks.practice.practice11;

import java.util.Scanner;

public class Task01 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter the first number");
        int number1 = scanner.nextInt();

        System.out.println("Please enter the second number");
        int number2 = scanner.nextInt();
        scanner.close();

        if (number1 <= number2)
        {
            for (int a = number1; a <= number2; a++)
            {
                System.out.print(a + " ");
            }
        }
        else
        {
            for (int a = number2; a <= number1; a++)
            {
                System.out.print(a + " ");
            }
        }


    }
}
