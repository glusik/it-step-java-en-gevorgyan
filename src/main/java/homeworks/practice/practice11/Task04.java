package homeworks.practice.practice11;

import java.util.Scanner;

public class Task04 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter the first number");
        int number1 = scanner.nextInt();

        System.out.println("Please enter the second number");
        int number2 = scanner.nextInt();
        scanner.close();

        int sum = 0;
        int countOfNumbers = 0;
        if (number1 <= number2)
        {
            for (int a = number1; a <= number2; a++)
            {
                sum = sum + a;
                countOfNumbers++;
            }
        }
        else
        {
            for (int a = number2; a <= number1; a++)
            {
                sum = sum + a;
                countOfNumbers++;
            }
        }

        int arithmeticMean = sum/countOfNumbers;

        System.out.println("The sum of the numbers in the specified range is " + sum);
        System.out.println("The arithmetic mean of the numbers in the specified range is " + arithmeticMean);
    }
}
