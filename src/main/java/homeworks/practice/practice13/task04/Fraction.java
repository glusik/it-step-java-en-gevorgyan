package homeworks.practice.practice13.task04;

public class Fraction {
    private int numerator;
    private int denominator;

    public Fraction(int numerator, int denominator) {
        this.numerator = numerator;
        this.denominator = denominator;
    }

    public int getNumerator() {
        return numerator;
    }

    public void setNumerator(int numerator) {
        this.numerator = numerator;
    }

    public int getDenominator() {
        return denominator;
    }

    public void setDenominator(int denominator) {
        this.denominator = denominator;
    }

    //method for adding fractions
    public Fraction sum(Fraction fraction) {
        int rNumerator = 0;
        int rDenominator = 0;
        Fraction result = new Fraction(rNumerator, rDenominator);

        int num = 0;
        while (num <= denominator * fraction.denominator) {
            num++;
            if (num % denominator == 0 && num % fraction.denominator == 0) {
                result.denominator = num;
                break;
            }
        }

        result.numerator = numerator * (result.denominator / denominator) + fraction.numerator * (result.denominator / fraction.denominator);
        return result;
    }

    // method for subtracting fractions
    public Fraction subtraction(Fraction fraction) {
        int rNumerator = 0;
        int rDenominator = 0;
        Fraction result = new Fraction(rNumerator, rDenominator);

        int num = 0;
        while (num <= denominator * fraction.denominator) {
            num++;
            if (num % denominator == 0 && num % fraction.denominator == 0) {
                result.denominator = num;
                break;
            }
        }

        result.numerator = numerator * (result.denominator / denominator) - fraction.numerator * (result.denominator / fraction.denominator);
        return result;
    }

    // method for multiplying fractions
    public Fraction multiplication(Fraction fraction) {
        int rNumerator = 0;
        int rDenominator = 0;
        Fraction result = new Fraction(rNumerator, rDenominator);

        result.numerator = numerator * fraction.numerator;
        result.denominator = denominator * fraction.denominator;

        int min = 0;
        if (result.numerator <= result.denominator) {
            min = result.numerator;
        } else {
            min = result.denominator;
        }

        int num = 1;
        while (num <= min) {
            num++;
            if (result.numerator % num == 0 && result.denominator % num == 0) {
                result.numerator = result.numerator / num;
                result.denominator = result.denominator / num;
                break;
            }

        }
        return result;
    }
    // method for dividing fractions
    public Fraction division(Fraction fraction) {
        int rNumerator = 0;
        int rDenominator = 0;
        Fraction result = new Fraction(rNumerator, rDenominator);

        result.numerator = numerator * fraction.denominator;
        result.denominator = denominator * fraction.numerator;

        int min = 0;
        if (result.numerator <= result.denominator) {
            min = result.numerator;
        } else {
            min = result.denominator;
        }

        int num = 1;
        while (num <= min) {
            num++;
            if (result.numerator % num == 0 && result.denominator % num == 0) {
                result.numerator = result.numerator / num;
                result.denominator = result.denominator / num;
                break;
            }
        }
        return result;
    }
}
