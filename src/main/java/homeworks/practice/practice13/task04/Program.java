package homeworks.practice.practice13.task04;

public class Program {
    public static void main(String[] args) {
        Fraction fraction1 = new Fraction(5,6);
        Fraction fraction2 = new Fraction(4,3);

        System.out.println(fraction1.sum(fraction2).getNumerator());
        System.out.println(fraction1.sum(fraction2).getDenominator());


        System.out.println(fraction1.subtraction(fraction2).getNumerator());
        System.out.println(fraction1.subtraction(fraction2).getDenominator());

        System.out.println(fraction1.multiplication(fraction2).getNumerator());
        System.out.println(fraction1.multiplication(fraction2).getDenominator());

        System.out.println(fraction1.division(fraction2).getNumerator());
        System.out.println(fraction1.division(fraction2).getDenominator());


    }
}
