package homeworks.practice.practice13.task02;

public class City {
    private String cityName;
    private String regionName;
    private String countryName;
    private int numOfResidents;
    private String postcode;
    private String areaCode;

    // implementing access through individual fields
    public City(String cityName, String regionName, String countryName, int numOfResidents, String postcode, String areaCode) {
        this.cityName = cityName;
        this.regionName = regionName;
        this.countryName = countryName;
        this.numOfResidents = numOfResidents;
        this.postcode = postcode;
        this.areaCode = areaCode;
    }

    //class methods for data input and output
    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public int getNumOfResidents() {
        return numOfResidents;
    }

    public void setNumOfResidents(int numOfResidents) {
        this.numOfResidents = numOfResidents;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }
}
