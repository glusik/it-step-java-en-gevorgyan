package homeworks.practice.revision;

import java.util.Arrays;

public class Task01 {
    public static void main(String[] args) {
        int[] testArray = {29,68,34,5,42};
        System.out.println(closestDist(testArray));
    }

    public static int closestDist(int[] numbers)
    {
        Arrays.sort(numbers);
        int diff = numbers[numbers.length-1] - numbers[numbers.length-2];
        for (int i = numbers.length - 1; i > 0; i--)
        {
           if ((numbers[i] - numbers[i-1]) < diff)
           {
               diff = numbers[i] - numbers[i-1];
           }
        }
        return diff;
    }

//    public static int closestDist(int[] numbers)
//    {
//        int diff = Math.abs(numbers[0] - numbers[1]);
//        for (int i=0; i<numbers.length; i++)
//        {
//            for (int j = 1; j<numbers.length; j++)
//            {
//                if (i!=j && Math.abs(numbers[i] - numbers[j]) < diff)
//                    diff = Math.abs(numbers[i] - numbers[j]);
//            }
//
//        }
//        return diff;
//    }
}
