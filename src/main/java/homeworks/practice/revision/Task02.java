package homeworks.practice.revision;

public class Task02 {
    public static void main(String[] args) {

        boolean isPrimeNum = true;
        for (int i = 2; i <= 100; i++) {

            for (int j = 2; j < Math.ceil(Math.sqrt(i)); j++)
                if (i % j == 0) {
                    isPrimeNum = false;
                    break;
                } else {
                    isPrimeNum = true;
                }
            if (isPrimeNum)
                System.out.println(i);
        }

}}
