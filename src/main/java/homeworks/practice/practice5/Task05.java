package homeworks.practice.practice5;

public class Task05 {
    public static void main(String[] args) {
        System.out.println(sumCalculation(1,100));

    }

    public static int sumCalculation(int a, int b)
    {
        int sum = 0;
        if (b > a) {
            for (int number = a; number <= b; number++) {
                sum = sum + number;
            }
        }
        else if (a > b)
        {
            for (int number = b; number <= a; number++) {
                sum = sum + number;
            }
        }
        else
        {
            sum = a;
        }
        return sum;
    }
}
