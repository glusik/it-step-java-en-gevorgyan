package homeworks.practice.practice5;

public class Task04 {
    public static void main(String[] args) {
        System.out.println(largestNumber(11,1,9,9));

    }

    public static double largestNumber(double a, double b, double c, double d)
    {
        double max = a;
       if (b >= a && b >= c && b >= d)
       {
           max = b;
       }
       else if (c >= a && c >= b && c >= d)
       {
           max = c;
       }
       else if (d >= a && d >= b && d >= c)
       {
           max = d;
       }
       return max;
    }
}
