package homeworks.practice.practice5;

public class Task02 {
    public static void main(String[] args) {
        evenNumbers(201,201);

        }

    public static void evenNumbers(int a, int b)
    {
        if (a < b)
        {
            for (int number = a; number <= b; number++)
            {
                if (number % 2 == 0) {
                    System.out.print(number + " ");
                }
            }
        }
        else if (b < a)
        {
            for (int number = b; number <= a; number++)
            {
                if (number % 2 == 0) {
                    System.out.print(number + " ");
                }
            }
        }
        else
        {
            if (a % 2 == 0)
            {
                System.out.println(a);
            }
        }
    }
}
