package homeworks.homework12;
import java.io.*;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.io.BufferedReader;

public class Task07 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter the path to the file");
        String filePath = scanner.nextLine();

        System.out.println("Please enter the forbidden words separated by a space");
        String forbiddenWords = scanner.nextLine();
        String[] forbWordsArray = forbiddenWords.split(" ");
        List<String> forbiddenList = Arrays.asList(forbWordsArray);

        try
        {
            File inputFile = new File(filePath);

            String outputFilePath = System.getProperty("user.dir") + File.separator + "temp.txt"; // creating a path for a temporary file
            File outputFile = new File(outputFilePath);

            FileReader fr = new FileReader(inputFile);
            FileWriter fw = new FileWriter(outputFile);

            BufferedReader br = new BufferedReader(fr);
            BufferedWriter bw = new BufferedWriter(fw);

            String s;
            int count = 0;

            while ((s = br.readLine()) != null)
            {
                String[] words = s.split(" "); // creating an array of words from text line
                for (String word : words)
                {
                    if (forbiddenList.contains(word))
                    {
                        count++;
                    }
                    else
                    {
                        bw.write(word);
                    }
                }
                bw.write('\n');
            }
            System.out.println("The forbidden words are: " + Arrays.toString(forbiddenList.toArray()));
            System.out.println("The forbidden words appeared " + count + " times.");

            fw.flush();
            bw.flush();

            br.close();
            bw.close();

            fr.close();
            fw.close();
            scanner.close();

            inputFile.delete(); // deleting the original file
            outputFile.renameTo(inputFile); // renaming the temporary file to the original file's name
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
