package homeworks.homework12;

import java.io.*;
import java.util.Scanner;

public class Task06 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter the path to the first file");
        String filePath1 = scanner.nextLine();
        System.out.println("Please enter the path to the second file");
        String filePath2 = scanner.nextLine();
        System.out.println("Please enter the path to the third file");
        String filePath3 = scanner.nextLine();
        System.out.println("Please enter the path to the fourth file");
        String filePath4 = scanner.nextLine();

        File file1 = new File(filePath1);
        File file2 = new File(filePath2);
        File file3 = new File(filePath3);
        File file4 = new File(filePath4);


        try {
            FileInputStream fis1 = new FileInputStream(file1);
            FileInputStream fis2 = new FileInputStream(file2);
            FileInputStream fis3 = new FileInputStream(file3);

            FileOutputStream fos = new FileOutputStream(file4);

            int available = fis1.available();
            byte[] byteArray1 = new byte[available];
            fis1.read(byteArray1);
            fos.write(byteArray1); // writing from the 1st file

            available = fis2.available();
            byte[] byteArray2 = new byte[available];
            fis2.read(byteArray2);
            fos.write(byteArray2); // writing from the 2nd file

            available = fis3.available();
            byte[] byteArray3 = new byte[available];
            fis3.read(byteArray3);
            fos.write(byteArray3); // writing from the 3rd file

            fis1.close();
            fis2.close();
            fis3.close();

            fos.flush();
            fos.close();
            scanner.close();

        }
        catch (IOException e)
        {
            e.printStackTrace();
        }


    }
}
