package homeworks.homework12;

import java.io.*;
import java.util.Scanner;

public class Task01 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter the path to the file");
        String filePath = scanner.nextLine();

        File inputFile = new File(filePath);

        try {
            FileReader fileReader = new FileReader(inputFile);
            BufferedReader br = new BufferedReader(fileReader);
            while (true)
            {
                String line = br.readLine();
                if (line == null)
                {
                    break;
                }
                System.out.println(line);
            }
            br.close();
            fileReader.close();
            scanner.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
