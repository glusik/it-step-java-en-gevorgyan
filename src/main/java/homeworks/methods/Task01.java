package homeworks.methods;

public class Task01 {
    public static void main(String[] args)
    {
        // creating a diagonal matrix
        System.out.println("Diagonal matrix: ");
        int[][] matrix1 = new int[3][3];
        printMatrix(diagonalMatrix(matrix1));

        // creating a zero matrix
        System.out.println();
        System.out.println("Zero matrix:");
        int[][] matrix2 = new int[3][3];
        printMatrix(zeroMatrix(matrix2));

        // summing the 2 matrices
        System.out.println();
        System.out.println("Sum of matrices:");
        printMatrix(sumMatrix(matrix1,matrix2));

        //multiplication of matrices
        System.out.println();
        System.out.println("Multiplication of matrices");
        printMatrix(multiplicationOfMatrices(matrix1,matrix2));

        // multiplication of a matrix by a scalar
        System.out.println();
        System.out.println("Multiplication of a matrix by a scalar:");
        printMatrix(scalarMultiplication(matrix1,3));





    }

    // Method for creating a diagonal matrix
    public static int[][] diagonalMatrix(int[][] dMatrix)
    {
        for (int i = 0;i<dMatrix.length;i++)
        {
            dMatrix[i][i] = 1;
        }
        return dMatrix;
    }

    // Method for creating a zero matrix
    public static int[][] zeroMatrix(int[][] zMatrix)
    {
        for (int i = 0; i < zMatrix.length;i++)
        {
            for (int j = 0; j < zMatrix.length;j++)
            {
            zMatrix[i][j] = 0;
            }
        }
        return zMatrix;
    }

    // Method for summing 2 matrices
    public static int[][] sumMatrix(int[][] matrix1,int[][] matrix2)
    {
        int a = matrix1.length;
        int[][] result = new int[a][a];
        for (int i = 0; i < matrix1.length; i++)
        {
            for (int j = 0; j < matrix1.length;j++)
            {
                result[i][j] = matrix1[i][j] + matrix2[i][j];
            }
        }
        return result;
    }

    //Method for printing a matrix to console
    public static void printMatrix(int[][] matrix)
    {
        for (int[] array:matrix)
        {
            for (int element:array)
            {
                System.out.print(element);
            }
            System.out.println();
        }
    }

    //Method for multiplication of matrices
    public static int[][] multiplicationOfMatrices(int[][] matrix1, int[][] matrix2)
    {
        int a = matrix1.length;
        int[][] result = new int[a][a];
        for (int i = 0; i < matrix1.length; i++)
        {
            for (int j = 0; j < matrix1.length; j++)
            {
                result[i][j] = 0;
                for (int k = 0; k < matrix1.length; k++)
                {
                    result[i][j] += matrix1[i][k]*matrix2[k][j];
                }
            }
        }
        return result;
    }

    //Method for scalar multiplication of a matrix
    public static int[][] scalarMultiplication(int[][] matrix, int scalar)
    {
       int a = matrix.length;
       int[][] result = new int[a][a];
       for (int i = 0; i < a; i++) {
           for (int j = 0; j < a; j++)
           {
               result[i][j] = matrix[i][j]*a;
           }
       }
       return result;
    }


    }
