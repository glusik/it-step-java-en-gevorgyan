package homeworks.methods;

public class Task02 {
    public static void main(String[] args) {

        int [] array = {1,2,3};
        String[] stringArray = {"one", "two", "three"};
        int[][] intMatrix = {{0,0,2},{1,0,0}};
        float[][] floatMatrix = {{1.3f,1.5f,4.1f},{.5f,2.4f,3.7f}};

        // one-dimensional array of the int type
        overloadedMethod(array);


        //one-dimensional array of the String type
        overloadedMethod(stringArray);

        //two-dimensional array of the int type
        overloadedMethod(intMatrix);
        System.out.println();

        //two-dimensional array of the float type
        overloadedMethod(floatMatrix);





    }

    public static void overloadedMethod(int[] array)
    {
        for (int element:array)
        {
            System.out.print(element + " ");
        }
        System.out.println();
        System.out.println();
    }

    public static void overloadedMethod(String[] stringArray){
        for (String element:stringArray)
        {
            System.out.print(element + " ");
        }
        System.out.println();
        System.out.println();
    }

    public static void overloadedMethod(int[][] multiDArray){
        for (int[] nested:multiDArray)
        {
            for (int element:nested)
            {
                System.out.print(element + " ");
            }
            System.out.println();
        }
    }

    public static void overloadedMethod(float[][] multiDArray){
        for (float[] nested:multiDArray)
        {
            for (float element:nested)
            {
                System.out.print(element + " ");
            }
            System.out.println();
        }
    }

}

