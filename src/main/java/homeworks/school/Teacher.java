package homeworks.school;

public class Teacher {
    String firstName;
    String lastName;

    public String getFullName(){
        String fullName = firstName + " " + lastName;
        return fullName;
    }

    public void printInfo(){
        System.out.println("Teacher: " + getFullName()  );
    }



}
