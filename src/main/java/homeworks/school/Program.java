package homeworks.school;

public class Program {
    public static void main(String[] args) {
        Student student1 = new Student();
        Student student2 = new Student();
        Student student3 = new Student();
        Student student4 = new Student();
        Student student5 = new Student();
        Student student6 = new Student();
        Student student7 = new Student();
        Student student8 = new Student();

        student1.firstName = "Harry";
        student1.lastName = "Potter";
        student1.age = 11;
        student1.isMan = true;
        student1.grades = new int[] {2, 1, 2, 2, 3};


        student2.firstName = "Hermiona";
        student2.lastName = "Granger";
        student2.age = 12;
        student2.isMan = false;
        student2.grades = new int[] {1, 1, 1, 1, 1};


        student3.firstName = "Draco";
        student3.lastName = "Malfoy";
        student3.age = 11;
        student3.isMan = true;
        student3.grades = new int[] {3, 3, 3, 4};


        student4.firstName = "Voncent";
        student4.lastName = "Crabbe";
        student4.age = 14;
        student4.isMan = true;
        student4.grades = new int[] {5, 4, 5, 4, 5, 4, 5};


        student5.firstName = "Cedric";
        student5.lastName = "Diggory";
        student5.age = 15;
        student5.isMan = true;
        student5.grades = new int[] {1, 2, 1, 2, 1, 2};


        student6.firstName = "Susan";
        student6.lastName = "Bones";
        student6.age = 12;
        student6.isMan = false;
        student6.grades = new int[] {3, 4, 5, 3, 4, 5};


        student7.firstName = "Luna";
        student7.lastName = "Lovegood";
        student7.age = 14;
        student7.isMan = false;
        student7.grades = new int[] {3, 3, 3, 4, 5};


        student8.firstName = "Cho";
        student8.lastName = "Chang";
        student8.age = 15;
        student8.isMan = false;
        student8.grades = new int[] {1, 2, 3, 4, 5};

        student1.printInfo();
        student2.printInfo();
        student3.printInfo();
        student4.printInfo();
        student5.printInfo();
        student6.printInfo();
        student7.printInfo();
        student8.printInfo();

        Teacher teacher1 = new Teacher();
        Teacher teacher2 = new Teacher();
        Teacher teacher3 = new Teacher();
        Teacher teacher4 = new Teacher();

        teacher1.firstName = "Minerva";
        teacher1.lastName = "McGonagall";

        teacher2.firstName = "Severus";
        teacher2.lastName = "Snape";

        teacher3.firstName = "Filius";
        teacher3.lastName = "Flitwick";

        teacher4.firstName = "Pomona";
        teacher4.lastName = "Sprout";

        teacher1.printInfo();
        teacher2.printInfo();
        teacher3.printInfo();
        teacher4.printInfo();
        System.out.println();


        House house1 = new House();
        House house2 = new House();
        House house3 = new House();
        House house4 = new House();

        house1.name = "Gryffindor";
        house2.name = "Hufflepuff";
        house3.name = "Slytherin";
        house4.name = "Ravenclaw";

        house1.teacher = teacher1;
        house2.teacher = teacher4;
        house3.teacher = teacher2;
        house4.teacher = teacher3;

        house1.students = new Student[] {student1,student2};
        house2.students = new Student[] {student5,student6};
        house3.students = new Student[] {student3,student4};
        house4.students = new Student[] {student7,student8};

        house1.printAllStudentsFullNames();
        house2.printAllStudentsFullNames();
        house3.printAllStudentsFullNames();
        house4.printAllStudentsFullNames();

        house1.printInfo();
        house2.printInfo();
        house3.printInfo();
        house4.printInfo();

        house1.printFullInfo();
        house2.printFullInfo();
        house3.printFullInfo();
        house4.printFullInfo();

        School school = new School();
        school.name = "Hogwarts";
        school.state = "United Kingdom";
        school.houses = new House[] {house1, house2, house3, house4};
        school.printFullInfo();

    }
}
