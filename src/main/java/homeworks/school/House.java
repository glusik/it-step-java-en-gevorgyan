package homeworks.school;

public class House {
    String name;
    Teacher teacher;
    Student[] students;

    public Student getBestStudent(){

        int bestGrade = 0;
        Student bestStudent = students[0];
        for (Student student:students)
        {
            if (student.getAverageGrade() < bestStudent.getAverageGrade())
                bestStudent = student;
        }
//
        return bestStudent;
        }

    public void printAllStudentsFullNames()
    {
        for (Student student:students)
        {
            System.out.println(student.getFullName());
        }
        System.out.println();
    }

    public void printInfo()
    {
        System.out.println(name);
        System.out.println("Teacher: " + teacher.getFullName());
        System.out.println("Count of students: " + students.length);
        System.out.println();
    }

    public void printFullInfo()
    {
        printInfo();
        System.out.println("Best student: " + getBestStudent().getFullName());
        System.out.println("All students: " + '\n' + "=====================");
        printAllStudentsFullNames();
        System.out.println("=====================");
        System.out.println();
    }
    }

