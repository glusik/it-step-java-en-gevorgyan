package homeworks.school;

public class School {
    String name;
    String state;
    House[] houses;

public Student getBestStudent(){
    Student bestStudent = houses[0].getBestStudent();
    for (House house: houses)
    {
     if (house.getBestStudent().getAverageGrade() < bestStudent.getAverageGrade())
         bestStudent = house.getBestStudent();
    }
    return bestStudent;
}

public void printInfo(){
    System.out.println(name + '\n' + state + '\n' + "Number of houses: " + houses.length);
}

public void printFullInfo(){
    printInfo();
    System.out.println("Best student: " + getBestStudent().getFullName() + '\n' + "Houses: " + '\n');
    for (House house:houses)
    {
        house.printFullInfo();
    }

}
}
