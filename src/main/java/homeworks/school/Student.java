package homeworks.school;

public class Student {
    String firstName;
    String lastName;
    int age;
    boolean isMan;
    int[] grades;

    public String getFullName() {
       String fullName = firstName + " " + lastName;
       return fullName;
    }

    public double getAverageGrade(){
        double sum = 0;
        for (double grade:grades)
        {
            sum = sum + grade;
        }
        double avgGrade = sum/grades.length;
        return  avgGrade;
    }

    public void printGrades(){
        System.out.print("Grades: ");
        for (int element:grades)
        {
            System.out.print(element + " ");
        }
        System.out.println();
    }

    public void printInfo (){
        System.out.println(getFullName());
        System.out.println("Age: " + age);
        if (isMan)
            System.out.println("Sex: Man");
        else
            System.out.println("Sex: Woman");
        printGrades();
        System.out.println("Average grade: " + getAverageGrade());
        System.out.println();
    }


}
