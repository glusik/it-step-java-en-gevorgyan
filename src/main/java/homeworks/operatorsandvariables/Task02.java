package homeworks.operatorsandvariables;

public class Task02 {
    public static void main(String[] args) {

        //Defining an integer variable
        int n = 543;

        //Extracting the digits of the variable
        int a = n % 10;
        n = n / 10;
        int b = n % 10;
        n = n / 10;
        int c = n % 10;
        int s = a + b + c;

        //Calculating sum of the digits
        System.out.println(s);

    }
}
