package homeworks.operatorsandvariables;

import java.util.Date;

public class Task11 {
    public static void main(String[] args) {
        //Defining variables of current date and time, and the Christmas date and time
        Date now = new Date();
        Date xmas = new Date (120,11,25,00,00,00);

        // Calculating how many seconds are left till Christmas
        long millisecndsNow = now.getTime();
        long millisecondsXmas = xmas.getTime();
        long totalSeconds = (millisecondsXmas - millisecndsNow) / 1000;

        //Converting the seconds left to days, minutes and seconds
        int secondsLeft = (int) (totalSeconds % 60);
        int totalMinutes = (int) (totalSeconds / 60);
        int minutesLeft = totalMinutes % 60;
        int totalHours = totalMinutes / 60;
        int hoursLeft = totalHours % 24;
        int totalDays = totalHours / 24;

        System.out.println(totalDays + " days, " + hoursLeft + "hours, " + minutesLeft + "minutes, and " + secondsLeft + "seconds");
    }
}
