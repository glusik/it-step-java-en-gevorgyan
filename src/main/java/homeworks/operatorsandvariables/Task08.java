package homeworks.operatorsandvariables;

public class Task08 {
    public static void main(String[] args) {
        //Defining the speed of sound and the time between the flash and the clap of thunder
        final int speed = 343;
        final int time = 8;

        //Calculating the distance based on the two constants
        int distance = speed * time;

        System.out.println(distance);
    }
}
