package homeworks.operatorsandvariables;

public class Task10 {
    public static void main(String[] args) {
        //Defining the constants of weight and the height
        final double weight = 102;
        final double height = 179;

        //Calculating the ideal weight based on the constants
        double idealWeight = height - 110;

        //Calculating the difference between the current and the ideal weights
        double goal = idealWeight - weight;

        //Showing if there is a need to lose or gain weight and how much
        if (goal > 0){
            System.out.println("You need to gain " + goal + "kg!" );
        };
        if (goal == 0){
            System.out.println("You already have an ideal weight!");
        };
        if (goal < 0){
            System.out.println("You need to lose " + Math.abs(goal) + "kg!");
        }
    }
}
