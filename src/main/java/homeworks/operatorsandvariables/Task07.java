package homeworks.operatorsandvariables;

public class Task07 {
    public static void main(String[] args) {
        //defining the interest rate and the deposited amount
        double rate = 0.0235;
        int deposit = 300000;

        //calculating the amount after 2 years of keeping the deposit
        double sum = deposit * (1 + rate) * (1 + rate);

        System.out.println(sum);
    }
}
