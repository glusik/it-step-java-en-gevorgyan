package homeworks.operatorsandvariables;

public class Task03 {
    public static void main(String[] args) {
        //Defining a variable with decimal points
        double n = 9.24;

        //Rounding the variable to the nearest integer
        long r = Math.round (n);

        System.out.println(r);

    }
}
