package homeworks.operatorsandvariables;

public class Task06 {
    public static void main(String[] args) {
        // Defining the constants representing the height and the width of the rectangle
        final int w = 10;
        final int h = 6;

        //Calculating the perimeter and the are of the rectangle
        int p = 2 * w + 2 * 6;
        int a = w * h;

        System.out.println(p);
        System.out.println(a);
    }
}
