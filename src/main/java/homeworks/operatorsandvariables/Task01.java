package homeworks.operatorsandvariables;

public class Task01 {
    public static void main(String[] args) {
       // Defining integer variables
        int x = 9;
        int y = 4;

        // Calculating the integer result after dividing the variables
        int d = x / y;

        // Calculating the remainder of the division
        int r = x % y;

        // Calculating the square root of variable x
        double sr = Math.sqrt(x);

        System.out.println(d);
        System.out.println(r);
        System.out.println(sr);

    }
}
