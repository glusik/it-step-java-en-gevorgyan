package homeworks.operatorsandvariables;

public class Task12 {
    public static void main(String[] args) {
        // Defining the amount of pupils
        int n = 30;

        //Defining the percentage of underweight pupils
        double u = 1;

        //Calculating the amount of underweight pupils
        int up = (int) Math.ceil(n * u / 100);


        //Calculating how many glasses of milk is needed
        final int gmilk = up;

        //Calculating how many ml of milk is needed
        int milkml = gmilk * 200;

        //Calculating how many milk packages are needed
        int milkToBuy = milkml / 900;
        if (milkml % 900 != 0){
            milkToBuy = milkToBuy + 1;
        }

        //Calculating how many pastries are needed
        double pastry = n + up;

        System.out.println(milkToBuy + " milk packages are needed");
        System.out.println(pastry + " pastries are needed");






    }
}
