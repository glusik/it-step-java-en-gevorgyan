package homeworks.operatorsandvariables;

public class Task05 {
    public static void main(String[] args) {
        //Defining the constants: radius of the circle and Pi
        final int R = 4;
        final double Pi = Math.PI;

        // Calculating the are and the circumference of the circle
        double area = Pi * R * R;
        double c = 2 * Pi * R;

        System.out.println(area);
        System.out.println(c);
    }
}
