package homeworks.operatorsandvariables;

public class Task09 {
    public static void main(String[] args) {
        // Defining a variable
        double n = 3.70;

        // Casting the variable to integer type
        int i = (int) n;

        //Calculating the result of dividing the variable by an integer variable
        double d = n / i;

        //Checking if the variable has a real part
        System.out.println(d != 1.0);
    }
}
