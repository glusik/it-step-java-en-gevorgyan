package meetings.meeting4;

public class ProgramMatrix {
    public static void main(String[] args) {
        // Introducing matrix 1
        int [] [] matrix1 = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };
        // Introducing matrix 2
        int [] [] matrix2 = {
                {9, 8, 7},
                {6, 5, 4},
                {3, 2, 1}
        };

        int i1 = 0;
        while (i1 < matrix1.length)
        {
            System.out.println();

            int i2 = i1;

            for (int j1 = 0; j1 < matrix1[i1].length; j1++)
            {
                int j2 = j1;

                System.out.print(matrix1[i1][j1] + matrix2[i2][j2] + " ");

            }
            i1++;
        }
    }
}
