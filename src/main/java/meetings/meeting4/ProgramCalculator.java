package meetings.meeting4;

import java.util.Scanner;

public class ProgramCalculator {
    public static void main(String[] args) {
        // Asking the use to enter the first number
        Scanner inputNumber1 = new Scanner(System.in);
        System.out.println("Please enter the first number");
        double firstNumber = inputNumber1.nextDouble();

        // Asking the user to enter the operation
        Scanner inputOperation = new Scanner(System.in);
        System.out.println("Please enter one of the following operations: +, -, *, /, %");
        String operation = inputOperation.nextLine();

        //Asking the user to enter the second number
        Scanner inputNumber2 = new Scanner(System.in);
        System.out.println("Please enter the second number");
        double secondNumber = inputNumber2.nextDouble();

        // Getting the operation character
        char operationChar = operation.charAt(0);

        // Printing the result according to the operation type
        if (operationChar == '\u002B')
            System.out.println(firstNumber + secondNumber);
        else if (operationChar == '\u002D')
            System.out.println(firstNumber - secondNumber);
        else if (operationChar == '\u002A')
            System.out.println(firstNumber * secondNumber);
        else if (operationChar == '\u002F')
            System.out.println(firstNumber / secondNumber);
        else if (operationChar == '\u0025')
            System.out.println(firstNumber % secondNumber);
        else
            System.out.println("Invalid operation");
    }
}
