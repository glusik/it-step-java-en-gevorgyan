package meetings.meeting4;

public class Matrix {
    public static void main(String[] args) {

        int [][] first = {{1,2,3}, {4,5, 6}, {7, 8, 9}};

        for (int[] nested : first)
        {
            for (int number: nested)
            {
                System.out.print(number + " ");
            }
            System.out.println();
        }

        System.out.println();


        int [][] second = {{9,8,7}, {6,5,4}, {3,2,1}};

        for (int[] nested : second)
        {
            for (int number: nested)
            {
                System.out.print(number + " ");
            }
            System.out.println();
        }

        System.out.println();

        int[][] result = new int[3][3];

        for (int i=0; i<result.length;i++)
        {
            for (int j=0; j<result.length;j++)
            {
                result[i][j] = first[i][j] + second[i][j];
            }
        }

        for (int[] nested : result)
        {
            for (int number: nested)
            {
                System.out.print(number + " ");
            }
            System.out.println();
        }


    }
}
