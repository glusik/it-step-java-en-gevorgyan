package meetings.meeting4;

public class ProgramParrot {
    public static void main(String[] args) {
        // Entering text
        String text = "bla bla bla";

        // Printing the text twice
        System.out.println(text);
        System.out.println(text);
    }
}
