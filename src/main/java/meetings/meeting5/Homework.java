package meetings.meeting5;

public class Homework {
    public static void main(String[] args) {
        // Printing information about human
        printInformation("Lusine", "Gevorgyan", 26);

        System.out.println();
        System.out.println("-----------------");
        System.out.println();

        // Printing the result of 4 numbers' multiplication
        System.out.println(multiplication(13, 1, 4, 62));

        System.out.println();
        System.out.println("-----------------");
        System.out.println();

        // Checking if the argument is an odd number
        System.out.println(evenOrOdd(13));

        System.out.println();
        System.out.println("-----------------");
        System.out.println();

        // Printing an array
        int[] array1 = {1,2,3,4,5};
        Array(array1);

        System.out.println();
        System.out.println("-----------------");
        System.out.println();

        // Printing a matrix
        int[][] myMatrix = {{1,2,3}, {4,5,6}};
        printMatrix(myMatrix);

        System.out.println();
        System.out.println("-----------------");
        System.out.println();

        // Summing 2 matrices
        int[][] matrix1 = {{1,1,1},{2,2,2},{4,4,4},{0,0,0}};
        int[][] matrix2 = {{3,3,3},{2,2,2},{0,0,0},{4,4,4}};
        sumMatrix(matrix1,matrix2);

    }

    public static void printInformation(String firstName, String lastName, int age)
    {
        System.out.println("First name: " + firstName);
        System.out.println("Last name: " + lastName);
        System.out.println("Age: " + age);
    }

    public static int multiplication (int no1, int no2, int no3, int no4)
    {
        int result = no1 * no2 * no3 * no4;
        return result;
    }

    public static boolean evenOrOdd(int number)
    {
        boolean result;
        if (number % 2 == 0)
            result = false;
        else
            result = true;
          return result;
    }

    public static void Array(int[] array)
    {
        for (int number: array)
        {
            System.out.print(number + " ");
        }
    }

    public static void printMatrix(int[][] matrix)
    {
        for (int[] nested :matrix)
        {
            for (int element: nested)
            {
                System.out.print(element + " ");
            }
            System.out.println();
        }
    }

    public static int[][] sumMatrix(int[][] matrix1, int[][] matrix2)
    {
        int a = matrix1.length;
        int b = matrix1[0].length;

        int[][] sum = new int [a][b];

        for(int i = 0; i < sum.length;i++)
        {
            for (int j = 0; j < sum[i].length; j++)
            {
                sum[i][j] = matrix1[i][j] + matrix2[i][j];
            }
        }
        printMatrix(sum);
        return sum;
    }






}
